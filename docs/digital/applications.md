## Digital Application Space

### Create an Application Tree

```js
import { Applications } from './digital/applications';

const app = new Application( `Amazing Animals Online`, `Scholastic Go!`, `Scholastic Library Publishing`, `Allison Henderson`, `Y`, `Y` ); // => { CMSProducts: [ { amazingAnimalsOnline: [Object] } ] }
```
