## Install

```sh
## Clone the repo
$ git clone https://github.com/sajjadhossain/scholastic-digital-application-tools.git

## Install all dependencies
$ npm install
```
