## Boilerplate

* Written JavaScript ES6
* Transpiled with [babel](https://babeljs.io/)
* Build with [gulp](http://gulpjs.com/)
* Test with [mocha](https://mochajs.org/), [istanbul](https://istanbul.js.org/) & [nyc](https://github.com/istanbuljs/nyc)
* Lint with [idiomatic](https://github.com/rwaldron/idiomatic.js/) & [eslint](http://eslint.org/) (using JS code style idiomatic)
* Documentation with [verb](https://github.com/verbose/verb)
