### Test

```sh
## Unit tests
$ npm run test:unit

## E2E tests
$ npm run test:e2e

## All tests
$ npm run test
```

### Test coverage

```sh
$ npm run coverage
```

### Linting

```sh
## Lint `./src`
$ npm run lint

## Lint './test`
$ npm run lint:test
```
