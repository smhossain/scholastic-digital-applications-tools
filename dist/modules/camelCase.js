'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var camelCase = function camelCase(str) {
  return str.replace(/\s(.)/g, function upperCase($1) {
    return $1.toUpperCase();
  }).replace(/\s/g, '').replace(/^(.)/, function lowerCase($1) {
    return $1.toLowerCase();
  });
};

exports.camelCase = camelCase;
//# sourceMappingURL=camelCase.js.map
