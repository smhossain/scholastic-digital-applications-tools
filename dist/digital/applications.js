'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Applications = undefined;

var _application = require('./model/application');

var CMSProducts = void 0;
var AmazingAnimalsOnline = new _application.Application('Amazing Animals Online', 'Scholastic Go!', 'Scholastic Library Publishing', 'Allison Henderson', 'Y', 'Y');
var AmericaTheBeautifulOnline = new _application.Application('America The Beautiful Online', 'Scholastic Go!', 'Scholastic Library Publishing', 'Allison Henderson', 'Y', 'Y');
var BookFlix = new _application.Application('BookFlix', null, 'Scholastic Library Publishing', 'Allison Henderson', 'Y', 'Y');
var ComprehensionClubs = new _application.Application('Comprehension Clubs', null, 'SCCG', 'Allison Henderson', 'Y', 'Y');
var CoreClicks = new _application.Application('Core Clicks', null, 'Classroom Magazines', 'Danielle Mirsky, Maren Misevich', 'Y', 'Y');
var Expert21 = new _application.Application('Expert 21', null, null, 'HMH', 'Y', 'Y');
var ExpertSpace = new _application.Application('Expert Space', null, null, 'Beth Polcari', 'Y', 'Y');
var ExpertSpaceCanada = new _application.Application('Expert Space Canada', null, 'Scholastic Education', 'Mary Falconer', 'Y', 'Y');
var EncyclopediaAmericanaOnline = new _application.Application('Encyclopedia Americana Online', null, 'Scholastic Library Publishing', 'Allison Henderson', 'Y', 'Y');
var FactsForNow = new _application.Application('Facts for Now', 'Scholastic Go!', 'SCCG', 'Janelle Cherrington', 'Y', 'Y');
var FreedomFlix = new _application.Application('FreedomFlix', null, 'Scholastic Library Publishing', 'Allison Henderson', 'Y', 'Y');

CMSProducts = [AmazingAnimalsOnline, AmericaTheBeautifulOnline, BookFlix, ComprehensionClubs, CoreClicks, Expert21, ExpertSpace, ExpertSpaceCanada, EncyclopediaAmericanaOnline, FactsForNow, FreedomFlix];

var Applications = { CMSProducts: CMSProducts };

exports.Applications = Applications;
//# sourceMappingURL=applications.js.map
