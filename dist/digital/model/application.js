'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Application = undefined;

var _camelCase = require('../../modules/camelCase');

var Application = function Application(name, reference, businessDivision, businessOwner, cms, website) {
  var applicationObject = {};
  var camelCaseName = (0, _camelCase.camelCase)(name);

  applicationObject[camelCaseName] = {
    reference: reference ? reference : null,
    name: name ? name : null,
    businessDivision: businessDivision ? businessDivision : null,
    businessOwner: businessOwner ? businessOwner : null,
    cms: cms === 'Y' ? true : false,
    website: website === 'Y' ? true : false
  };

  return applicationObject;
};

exports.Application = Application;
//# sourceMappingURL=application.js.map
