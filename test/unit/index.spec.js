/*global describe, expect, it, beforeEach*/
import { Application } from './../../src/digital/model/application';

describe( 'unit:Application', () => {

  let app;

  beforeEach( () => {
    app = new Application( `Amazing Animals Online`, `Scholastic Go!`, `Scholastic Library Publishing`, `Allison Henderson`, `Y`, `Y` );
  } );

  it( 'should have an object representing an application', () => {
    expect( app.amazingAnimalsOnline ).to.exist;
    expect( app.amazingAnimalsOnline ).to.be.a( 'object' );
  } );

  it( 'should have a reference property', () => {
    expect( app.amazingAnimalsOnline ).to.have.a.property( 'reference', 'Scholastic Go!' );
  } );

  it( 'should have a name property', () => {
    expect( app.amazingAnimalsOnline ).to.have.a.property( 'name', 'Amazing Animals Online' );
  } );

  it( 'should have a businessDivision property', () => {
    expect( app.amazingAnimalsOnline ).to.have.a.property( 'businessDivision', 'Scholastic Library Publishing' );
  } );

  it( 'should have a businessOwner property', () => {
    expect( app.amazingAnimalsOnline ).to.have.a.property( 'businessOwner', 'Allison Henderson' );
  } );

  it( 'should have a cms property', () => {
    expect( app.amazingAnimalsOnline ).to.have.a.property( 'cms', true );
  } );

  it( 'should have a website property', () => {
    expect( app.amazingAnimalsOnline ).to.have.a.property( 'website', true );
  } );
} );
