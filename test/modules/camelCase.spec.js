/*global describe, expect, it, beforeEach*/
import { camelCase } from './../../src/modules/camelCase';

describe( 'module:camelCase', () => {

  let mod;

  beforeEach( () => {
    mod = camelCase( `Amazing Animals Online` );
  } );

  it( 'should return a string', () => {
    expect( mod ).to.be.a( 'string' );
  } );

  it( 'should return passed string in camel case', () => {
    expect( mod ).to.equal( 'amazingAnimalsOnline' );
  } );
} );
