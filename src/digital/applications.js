import { Application } from './model/application';

let CMSProducts;
const AmazingAnimalsOnline = new Application( 'Amazing Animals Online', 'Scholastic Go!', 'Scholastic Library Publishing', 'Allison Henderson', 'Y', 'Y' );
const AmericaTheBeautifulOnline = new Application( 'America The Beautiful Online', 'Scholastic Go!', 'Scholastic Library Publishing', 'Allison Henderson', 'Y', 'Y' );
const BookFlix = new Application( 'BookFlix', null, 'Scholastic Library Publishing', 'Allison Henderson', 'Y', 'Y' );
const ComprehensionClubs = new Application( 'Comprehension Clubs', null, 'SCCG', 'Allison Henderson', 'Y', 'Y' );
const CoreClicks = new Application( 'Core Clicks', null, 'Classroom Magazines', 'Danielle Mirsky, Maren Misevich', 'Y', 'Y' );
const Expert21 = new Application( 'Expert 21', null, null, 'HMH', 'Y', 'Y' );
const ExpertSpace = new Application( 'Expert Space', null, null, 'Beth Polcari', 'Y', 'Y' );
const ExpertSpaceCanada = new Application( 'Expert Space Canada', null, 'Scholastic Education', 'Mary Falconer', 'Y', 'Y' );
const EncyclopediaAmericanaOnline = new Application( 'Encyclopedia Americana Online', null, 'Scholastic Library Publishing', 'Allison Henderson', 'Y', 'Y' );
const FactsForNow = new Application( 'Facts for Now', 'Scholastic Go!', 'SCCG', 'Janelle Cherrington', 'Y', 'Y' );
const FreedomFlix = new Application( 'FreedomFlix', null, 'Scholastic Library Publishing', 'Allison Henderson', 'Y', 'Y' );


CMSProducts = [
  AmazingAnimalsOnline,
  AmericaTheBeautifulOnline,
  BookFlix,
  ComprehensionClubs,
  CoreClicks,
  Expert21,
  ExpertSpace,
  ExpertSpaceCanada,
  EncyclopediaAmericanaOnline,
  FactsForNow,
  FreedomFlix
];

const Applications = { CMSProducts: CMSProducts };

export { Applications };
