import { camelCase } from '../../modules/camelCase';

const Application = ( name, reference, businessDivision, businessOwner, cms, website ) => {
  let applicationObject = {};
  let camelCaseName = camelCase( name );

  applicationObject[ camelCaseName ] = {
    reference: reference ? reference : null,
    name: name ? name : null,
    businessDivision: businessDivision ? businessDivision : null,
    businessOwner: businessOwner ? businessOwner : null,
    cms: ( cms === 'Y' ) ? true : false,
    website: ( website === 'Y' ) ? true : false
  };

  return applicationObject;
};

export { Application };
